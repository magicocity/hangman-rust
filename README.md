# hangman-rust
A simple game implemented in rust<br>
There are two word generation options:
1) The wordlength and contents are randomly generated so the word might be nonsense
2) A word will be randomly chosen from a list of words.
The guess limit is randomly generated but will be at least 5 guesses. 
If you hit the guess limit before guessing the word you will lose the game. 